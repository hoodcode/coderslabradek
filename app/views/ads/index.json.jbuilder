json.array!(@ads) do |ad|
  json.extract! ad, :title, :description, :locationstring, :owner_id
  json.url ad_url(ad, format: :json)
end
